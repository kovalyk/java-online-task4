package com.epam.task4.Game;

import com.epam.task4.Game.Controller.Battle;
import com.epam.task4.Game.Entity.Room;
import com.epam.task4.Game.Entity.SuperHero;


public class App {
    public static void main(String[] args) {
        SuperHero hero = new SuperHero();
        Room room = new Room();
        room.setRooms();
        room.showRooms();
        System.out.println("Number of possible death-rooms: " + Battle.calculateDeath(hero, room));
        System.out.println("To stay alive you have to choose the door : ");
        Battle.showWinStrategy(hero, room);
    }
}
