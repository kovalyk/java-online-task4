package com.epam.task4.Game.Entity;

import com.epam.task4.Game.Controller.Anticipation;

public class Artifact extends Anticipation {
    private final int MIN_RANGE = 10;
    private final int MAX_RANGE = 80;

    private int power;
    private String name;

    public Artifact() {
    }

    public int getPower() {
        return power = Door.getPowerRange(MIN_RANGE, MAX_RANGE);
    }

    public void setPower(int power) {
        this.power = power;
    }
}
