package com.epam.task4.Game.Entity;

import com.epam.task4.Game.Controller.Anticipation;

import java.util.Random;

public class Door {

    private Anticipation inside;

    private boolean isMonsterDoor;

   public Door(Anticipation inside) {
        this.inside = inside;
    }

    public static int getPowerRange(int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    public Anticipation getInside() {
        return this.inside;
    }

    public String getRoomInsiderName() {
        return this.inside.getClass().getSimpleName();
    }

    public void setInside(Anticipation inside) {
        this.inside = inside;
    }

    public boolean isMonsterDoor() {
        return isMonsterDoor;
    }

    public void setMonsterDoor(boolean monsterDoor) {
        isMonsterDoor = monsterDoor;
    }
}
