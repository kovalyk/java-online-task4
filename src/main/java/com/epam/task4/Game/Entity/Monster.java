package com.epam.task4.Game.Entity;

import com.epam.task4.Game.Controller.Anticipation;

public class Monster extends Anticipation {
    private final int MIN_RANGE = 5;
    private final int MAX_RANGE = 100;

    private int power;
    private String name;

    public Monster() {

    }

    public int getPower() {
        return power = Door.getPowerRange(MIN_RANGE, MAX_RANGE);
    }

    public void setPower(int power) {
        this.power = power;
    }
}
