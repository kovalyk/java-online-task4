package com.epam.task4.Game.Entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public final class Room {
    private List<Door> doors;
    private Random random;

    public Room() {
    }

    public void setRooms() {
        doors = new ArrayList();
        int result;
        for (int i = 0; i < 10; i++) {
            random = new Random();
            result = random.nextInt(100) + 1;
            if (result >= 50) {
                Door door = new Door(new Artifact());
                door.setMonsterDoor(false);
                doors.add(door);
            } else {
                Door door = new Door(new Monster());
                door.setMonsterDoor(true);
                doors.add(door);
            }
        }
    }

    public List<Door> getInsiders() {
        return doors;
    }

    public void showRooms() {
        int counter = 1;
        for (Door door : doors) {
            System.out.println(
                    "Behind the door " + counter + " : " + door.getRoomInsiderName() + " with power: " + door
                            .getInside().getPower());
            counter++;
        }
    }

    public List<Door> getDoors() {
        return doors;
    }
}