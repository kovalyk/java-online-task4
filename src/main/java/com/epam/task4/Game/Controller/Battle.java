package com.epam.task4.Game.Controller;

import com.epam.task4.Game.Entity.*;

import java.util.ArrayList;
import java.util.List;

public class Battle {
    public static Monster monster;

    private Battle() {
    }

    public static int calculateDeath(SuperHero hero, Room room) {
        int count = 0;
        for (Door door : room.getDoors()) {
            if (door.getInside().equals(monster)) {
                if (hero.getPower() < door.getInside().getPower()) {
                    count++;
                }
            }
        }
        return count;
    }

    public static void showWinStrategy(SuperHero hero, Room room) {
        List<Integer> list = new ArrayList();
        int counter = 1;
        int power = 25;
        for (Door door : room.getDoors()) {
            if (!door.isMonsterDoor()) {
                list.add(counter);
                power += door.getInside().getPower();
            }
            counter++;
        }
        counter = 1;
        for (Door door : room.getDoors()) {
            if (door.isMonsterDoor()) {
                if (door.getInside().getPower() <= power) {
                    list.add(counter);
                }
            }
            counter++;
        }
        for (Integer i : list) {
            System.out.println("Number " + i);
        }
    }
}