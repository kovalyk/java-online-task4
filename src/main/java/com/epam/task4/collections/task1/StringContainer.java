package com.epam.task4.collections.task1;

import java.util.Arrays;

public class StringContainer {

    private String[] strings = new String[10];

    private int size;

    public StringContainer() {
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getsStrings(int index) {
        return strings[index];
    }


    public void addString(String s) {
        if (size == strings.length) {
            sizeGrow();
        }
        this.strings[size++] = s;
    }

    private void sizeGrow() {
        String[] newStrings = new String[(int) (this.strings.length * 1.5)];
        for (int i = 0; i < strings.length; i++) {
            newStrings[i] = this.strings[i];
        }
        this.strings = newStrings;
    }

    public String getString(int index) {
        if ((index >= 0) && (index < size)) {
            return strings[size];
        } else {
            throw new IndexOutOfBoundsException("Index incorrect");
        }

    }

    public String[] getStrings() {
        return Arrays.copyOf(strings, size);
    }

    public void setStrings(String[] strings) {
        this.strings = strings;
        this.size = strings.length;
    }

}
