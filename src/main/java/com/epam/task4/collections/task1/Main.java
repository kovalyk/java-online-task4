package com.epam.task4.collections.task1;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        long start = System.nanoTime();
        StringContainer stringContainer = new StringContainer();
        String name = "One";
        for (int i = 0; i < 100; i++) {
            stringContainer.addString(name);
        }
        long end = System.nanoTime();

        System.out.println("Execution time, when adding elements to Array- " + (end - start));

        long listStart = System.nanoTime();
        ArrayList<String> comparingList = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            comparingList.add(name);
        }
        long listEnd = System.nanoTime();

        System.out.println("Execution time, when adding elements to List- " + (listEnd - listStart));

        System.out.println("=====================================================");

        start = System.nanoTime();
        for (int i = 0; i < 100; i++) {
            stringContainer.getString(i);
        }
        end = System.nanoTime();
        System.out.println("Execution time, when getting elements from Array- " + (end - start));

        listStart = System.nanoTime();

        for (int i = 0; i < 100; i++) {
            comparingList.get(i);
        }

        listEnd = System.nanoTime();
        System.out.println("Execution time, when getting elements from List- " + (listEnd - listStart));

    }
}
