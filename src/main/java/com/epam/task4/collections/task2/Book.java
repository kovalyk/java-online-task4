package com.epam.task4.collections.task2;

import java.util.Comparator;

public class Book implements Comparable<Book> {

    private String genre = "";
    private String bookName = "";

    public Book() {
    }

    public Book(String genre, String book) {
        this.genre = genre;
        this.bookName = book;
    }

    @Override
    public int compareTo(Book book) {
        return this.genre.compareTo(book.genre);
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    @Override
    public String toString() {
        return "Book{" +
                "genre='" + genre + '\'' +
                ", bookName='" + bookName + '\'' +
                '}';
    }

    private static class sortBookByName implements Comparator<Book> {

        @Override
        public int compare(Book bookByName1, Book bookByName2) {
            return bookByName1.getBookName().compareTo(bookByName2.getBookName());
        }
    }

}


