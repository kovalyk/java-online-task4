package com.epam.task4.collections.task2;

import java.util.*;

public class App {
    public static void main(String[] args) {
        List<Book> bookList = BookGenerator.generate(10);
        BookGenerator.reset();
        Book[] bookArray = BookGenerator.generate(10).toArray(new Book[10]);

        System.out.println();
        System.out.println("List with generated data: \n" + bookList);
        System.out.println("Array with generated data: \n" + Arrays.toString(bookArray));

        Collections.sort(bookList);
        Arrays.sort(bookArray);

        System.out.println();
        System.out.println("Sorted list by firstName: \n" + bookList);
        System.out.println("Sorted array by firstName: \n" + Arrays.toString(bookArray));

        Comparator<Book> genreComparator = new GenreComparator();

        Collections.sort(bookList, genreComparator);
        Arrays.sort(bookArray, genreComparator);

        System.out.println();
        System.out.println("Sorted list by secondName: \n" + bookList);
        System.out.println("Sorted array by secondName: \n" + Arrays.toString(bookArray));

        Scanner scanner = new Scanner(System.in);
        System.out.print("Please write a book name to search in the List: ");
        String genre = scanner.nextLine();
        Book book = new Book();
        book.setGenre(genre);

        int index = Collections.binarySearch(bookList, book, genreComparator);

        System.out.printf("Index of person with second name %s  is: %d\n", genre, index);

        scanner.close();
    }
}
