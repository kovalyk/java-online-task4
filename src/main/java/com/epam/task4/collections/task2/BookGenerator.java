package com.epam.task4.collections.task2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class BookGenerator {
    private static final BookGenerator instance = new BookGenerator();

    private static final List<String> bookNames = Arrays.asList("Ulysses", "In Search of Lost Time", "Don Quixote",
            "The Great Gatsby ", "Moby Dick", "One Hundred Years of Solitude ",
            "War and Peace", "Hamlet", "Lolita", "The Odyssey", "The Brothers Karamazov", "Madame Bovary", "Crime and Punishment");
    private static final List<String> genres = Arrays.asList("Fantasy", "Science fiction", "Western", "Romance", "Thriller",
            "Mystery", "Detective story", "Dystopia", "Memoir", "Biography", "Play", "Satire");

    private static List<Book> books = new ArrayList<>();

    private BookGenerator() {

    }

    public static Book generateOne() {
        Random rand = new Random();
        String randomBookName = bookNames.get(rand.nextInt(bookNames.size()));
        String randomGenre = genres.get(rand.nextInt(genres.size()));

        return new Book(randomBookName, randomGenre);
    }

    public static List<Book> generate(int count) {
        if (count > 0) {
            for (int i = 0; i < count; i++) {
                books.add(generateOne());
            }
        } else {
            System.out.println("Cannot be negative value");
        }
        return books;
    }


    public static void reset() {
        books = new ArrayList<>();
    }

    public static BookGenerator getInstance() {
        return instance;
    }
}
