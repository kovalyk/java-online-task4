package com.epam.task4.collections.task2;

import java.util.Comparator;

public class GenreComparator implements Comparator<Book> {
    @Override
    public int compare(Book bookByGenre1, Book bookByGenre2) {
        return bookByGenre1.getGenre().compareTo(bookByGenre1.getGenre());
    }
}

