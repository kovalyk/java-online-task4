package com.epam.task4.arrays.task2;

public class RemoveAllDuplicates {
    public static void main(String[] args) {
        int[] array = {1, 3, 5, 5, 7, 10, 5, 12, 12, 4};

        for (int i = 0; i < array.length; i++) {
            int count = 0;
            for (int j = 0; j < array.length; j++) {
                if ((array[i] == array[j]) && (i != j)) {
                    count++;
                }
            }
            if (count == 0) {
                System.out.print(array[i] + " ");
            }
        }
    }
}

