package com.epam.task4.arrays.task3;

public class RemoveClosestDuplicate {
    public static void main(String[] args) {
        int[] arr = {1, 3, 5, 5, 7, 10, 5, 12, 12, 10};

        for (int i = 0; i < arr.length - 1; i++) {

            if (arr[i] != arr[i + 1]) {
                System.out.print(arr[i] + " " );
            }
        }
    }
}
